return {
  "jose-elias-alvarez/null-ls.nvim",
  opts = function(_, opts)
    if type(opts.sources) == "table" then
      local nls = require("null-ls")
      local tsca = require("typescript.extensions.null-ls.code-actions")

      vim.list_extend(opts.sources, {
        nls.builtins.formatting.prettierd,
        nls.builtins.diagnostics.cmake_lint,
        nls.builtins.diagnostics.hadolint,
        nls.builtins.code_actions.gomodifytags,
        nls.builtins.code_actions.impl,
        nls.builtins.formatting.gofumpt,
        nls.builtins.formatting.goimports_reviser,
        nls.builtins.formatting.terraform_fmt,
        nls.builtins.diagnostics.terraform_validate,
        tsca,
      })
    end
  end,
}
