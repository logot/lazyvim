return {
  "williamboman/mason.nvim",
  optional = true,
  opts = function(_, opts)
    if type(opts.ensure_installed) == "table" then
      vim.list_extend(opts.ensure_installed, {
        "prettierd",
        -- "elixir-ls",
        "gomodifytags",
        "impl",
        "gofumpt",
        "delve",
        "goimports-reviser",
        "java-test",
        "java-debug-adapter",
        "codelldb",
        "js-debug-adapter",
        "astro-language-server",
      })
    end
  end,
}
