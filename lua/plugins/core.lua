return {
  { "mg979/vim-visual-multi", event = "BufRead" },
  { "tpope/vim-surround", event = "BufRead" }, -- mini_surround
  { "tpope/vim-repeat", event = "VeryLazy" },
}
