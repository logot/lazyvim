return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    if type(opts.ensure_installed) == "table" then
      vim.list_extend(opts.ensure_installed, {
        "c",
        "cpp",
        "cmake",
        "dockerfile",
        "elixir",
        "heex",
        "eex",
        "go",
        "gomod",
        "gowork",
        "gosum",
        "java",
        "json",
        "json5",
        "jsonc",
        "ninja",
        "python",
        "rst",
        "ron",
        "rust",
        "toml",
        "terraform",
        "hcl",
        "bibtex",
        "latex",
        "typescript",
        "tsx",
        "yaml",
        "astro",
      })
    end
    if type(opts.highlight.disable) == "table" then
      vim.list_extend(opts.highlight.disable, { "latex" })
    else
      opts.highlight.disable = { "latex" }
    end
  end,
}
