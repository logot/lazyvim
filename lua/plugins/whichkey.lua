require("lazyvim")
return {
  "folke/which-key.nvim",
  optional = true,
  opts = {
    defaults = {
      ["<leader>d"] = { name = "+debug" },
      ["<leader>da"] = { name = "+adapters" },
      ["<localLeader>l"] = { name = "+vimtex" },
      ["<leader>t"] = { name = "+test" },
    },
  },
}
