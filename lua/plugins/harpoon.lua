return {
  "theprimeagen/harpoon",
  keys = {
    {
      "<leader>a",
      function()
        require("harpoon.mark").add_file()
      end,
      desc = "Harpoon Add",
    },
    {
      "<C-e>",
      function()
        require("harpoon.ui").toggle_quick_menu()
      end,
      desc = "Harpoon Toggle",
      mode = { "n", "v" },
    },
    {
      "<C-p>",
      function()
        require("harpoon.ui").nav_prev()
      end,
      desc = "Harpoon Prev",
      mode = { "n", "v" },
    },
  },
}
