return {
  "hrsh7th/nvim-cmp",
  dependencies = {
    {
      "Saecki/crates.nvim",
      event = { "BufRead Cargo.toml" },
      config = true,
    },
    { "roobert/tailwindcss-colorizer-cmp.nvim", config = true },
    { "onsails/lspkind.nvim" },
  },
  ---@param opts cmp.ConfigSchema
  opts = function(_, opts)
    local cmp = require("cmp")
    opts.sources = cmp.config.sources(vim.list_extend(opts.sources, {
      { name = "crates" },
      -- { name = "codeium" },
    }))
    -- original LazyVim kind icon formatter
    local format_kinds = opts.formatting.format
    opts.formatting.format = function(entry, item)
      format_kinds(entry, item) -- add icons

      -- require("lspkind").cmp_format({
      --   mode = "symbol",
      --   maxwidth = 50,
      --   ellipsis_char = "...",
      --   symbol_map = { Codeium = "" },
      -- })

      return require("tailwindcss-colorizer-cmp").formatter(entry, item)
    end
  end,
}
